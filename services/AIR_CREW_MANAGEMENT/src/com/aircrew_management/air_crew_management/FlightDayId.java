/*Generated by WaveMaker Studio*/
package com.aircrew_management.air_crew_management;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class FlightDayId implements Serializable {

    private Date day;
    private String flightId;

    public Date getDay() {
        return this.day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public String getFlightId() {
        return this.flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FlightDay)) return false;
        final FlightDay flightDay = (FlightDay) o;
        return Objects.equals(getDay(), flightDay.getDay()) &&
                Objects.equals(getFlightId(), flightDay.getFlightId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDay(),
                getFlightId());
    }
}
