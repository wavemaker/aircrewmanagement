/*Generated by WaveMaker Studio*/
package com.aircrew_management.air_crew_management.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.file.model.Downloadable;

import com.aircrew_management.air_crew_management.Flight;
import com.aircrew_management.air_crew_management.FlightDay;
import com.aircrew_management.air_crew_management.UserDay;


/**
 * ServiceImpl object for domain model class Flight.
 *
 * @see Flight
 */
@Service("AIR_CREW_MANAGEMENT.FlightService")
public class FlightServiceImpl implements FlightService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FlightServiceImpl.class);

    @Autowired
	@Qualifier("AIR_CREW_MANAGEMENT.UserDayService")
	private UserDayService userDayService;

    @Autowired
	@Qualifier("AIR_CREW_MANAGEMENT.FlightDayService")
	private FlightDayService flightDayService;

    @Autowired
    @Qualifier("AIR_CREW_MANAGEMENT.FlightDao")
    private WMGenericDao<Flight, String> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Flight, String> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "AIR_CREW_MANAGEMENTTransactionManager")
    @Override
	public Flight create(Flight flight) {
        LOGGER.debug("Creating a new Flight with information: {}", flight);
        Flight flightCreated = this.wmGenericDao.create(flight);
        if(flightCreated.getUserDays() != null) {
            for(UserDay userDay : flightCreated.getUserDays()) {
                userDay.setFlight(flightCreated);
                LOGGER.debug("Creating a new child UserDay with information: {}", userDay);
                userDayService.create(userDay);
            }
        }

        if(flightCreated.getFlightDays() != null) {
            for(FlightDay flightDay : flightCreated.getFlightDays()) {
                flightDay.setFlight(flightCreated);
                LOGGER.debug("Creating a new child FlightDay with information: {}", flightDay);
                flightDayService.create(flightDay);
            }
        }
        return flightCreated;
    }

	@Transactional(readOnly = true, value = "AIR_CREW_MANAGEMENTTransactionManager")
	@Override
	public Flight getById(String flightId) throws EntityNotFoundException {
        LOGGER.debug("Finding Flight by id: {}", flightId);
        Flight flight = this.wmGenericDao.findById(flightId);
        if (flight == null){
            LOGGER.debug("No Flight found with id: {}", flightId);
            throw new EntityNotFoundException(String.valueOf(flightId));
        }
        return flight;
    }

    @Transactional(readOnly = true, value = "AIR_CREW_MANAGEMENTTransactionManager")
	@Override
	public Flight findById(String flightId) {
        LOGGER.debug("Finding Flight by id: {}", flightId);
        return this.wmGenericDao.findById(flightId);
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "AIR_CREW_MANAGEMENTTransactionManager")
	@Override
	public Flight update(Flight flight) throws EntityNotFoundException {
        LOGGER.debug("Updating Flight with information: {}", flight);
        this.wmGenericDao.update(flight);

        String flightId = flight.getFlightNum();

        return this.wmGenericDao.findById(flightId);
    }

    @Transactional(value = "AIR_CREW_MANAGEMENTTransactionManager")
	@Override
	public Flight delete(String flightId) throws EntityNotFoundException {
        LOGGER.debug("Deleting Flight with id: {}", flightId);
        Flight deleted = this.wmGenericDao.findById(flightId);
        if (deleted == null) {
            LOGGER.debug("No Flight found with id: {}", flightId);
            throw new EntityNotFoundException(String.valueOf(flightId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "AIR_CREW_MANAGEMENTTransactionManager")
	@Override
	public Page<Flight> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Flights");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "AIR_CREW_MANAGEMENTTransactionManager")
    @Override
    public Page<Flight> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Flights");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "AIR_CREW_MANAGEMENTTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service AIR_CREW_MANAGEMENT for table Flight to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "AIR_CREW_MANAGEMENTTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "AIR_CREW_MANAGEMENTTransactionManager")
    @Override
    public Page<UserDay> findAssociatedUserDays(String flightNum, Pageable pageable) {
        LOGGER.debug("Fetching all associated userDays");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("flight.flightNum = '" + flightNum + "'");

        return userDayService.findAll(queryBuilder.toString(), pageable);
    }

    @Transactional(readOnly = true, value = "AIR_CREW_MANAGEMENTTransactionManager")
    @Override
    public Page<FlightDay> findAssociatedFlightDays(String flightNum, Pageable pageable) {
        LOGGER.debug("Fetching all associated flightDays");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("flight.flightNum = '" + flightNum + "'");

        return flightDayService.findAll(queryBuilder.toString(), pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service UserDayService instance
	 */
	protected void setUserDayService(UserDayService service) {
        this.userDayService = service;
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service FlightDayService instance
	 */
	protected void setFlightDayService(FlightDayService service) {
        this.flightDayService = service;
    }

}

