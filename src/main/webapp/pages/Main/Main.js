Application.$controller("MainPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
        $scope.Variables.unassigned_flights.update();
        //$scope.Variables.selected_flight_details.dataSet = '';
        if (localStorage.getItem("toastFlag") == "true") {
            $scope.Variables.assignSuccessToast.notify();


            //last step
            localStorage.setItem("toastFlag", false);
            localStorage.setItem("flightId", "");

        }
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */

    };



    $scope.unassigned_flightsonSuccess = function(variable, data) {
        $scope.Widgets.label1.caption = data.content.length + " UNASSIGNED FLIGHTS";

    };


    $scope.livelist1Tap = function($event, $isolateScope) {
        //debugger;
        $scope.Variables.selected_flight_details.dataSet.PAGE = 'Main';
        //$scope.Variables.selected_flight_details.dataSet = $isolateScope.item;

    };


    $scope.button1Tap = function($event, $isolateScope) {
        var msg = 'Do you want reset flight data?';
        var text = confirm(msg)
        if (text === true) {
            $scope.Variables.executeResetQueryAvaliable.update();
            $scope.Variables.AIR_CREW_MANAGEMENTExecuteGet_crew_details.update();

        } else {
            // not confirm
        }
    };

}]);