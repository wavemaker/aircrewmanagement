Application.$controller("MembersPageController", ["$scope", function($scope) {
    "use strict";
    $scope.Variables.AIR_CREW_MANAGEMENTUserData.update();

    $scope.livelist3_1groupby = function(rowData) {
        /*
         * this function is iterated over each data object in the livelist dataSet collection the data will be grouped by what is returned from this function E.g. to group a collection of CGPA details under rounded figure CGPA return following return Math.floor(dataObject.cgpa)
         */
        if (rowData.role == "cc") {

            return "Cabin Crew";
        } else
        if (rowData.role == "fo") {
            return "First Officers";
        } else
        if (rowData.role == "captain") {
            return "Captain";
        } else {
            return "Crew Manager";
        }
    };


    $scope.mobile_navbar1Backbtnclick = function($event, $isolateScope) {
        $scope.Variables.AIR_CREW_MANAGEMENTUserData.setFilter('fullName', "%%");
        $scope.Variables.AIR_CREW_MANAGEMENTUserData.update();
        $scope.Widgets.mobile_navbar1.query = '';
        $scope.Variables.goToPage_Main.navigate();
    };


    $scope.mobile_navbar1Search = function($event, $isolateScope) {
        $scope.Variables.AIR_CREW_MANAGEMENTUserData.setFilter('fullName', $isolateScope.query);
        $scope.Variables.AIR_CREW_MANAGEMENTUserData.update();
    };

}]);