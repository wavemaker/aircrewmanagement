var _WM_APP_PROPERTIES = {
  "activeTheme" : "air-crew",
  "defaultLanguage" : "en",
  "displayName" : "AirCrewManagement",
  "homePage" : "Main",
  "name" : "AirCrewManagement",
  "platformType" : "MOBILE",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.0"
};